package edu.pe.cibertec.parcial.service;

import edu.pe.cibertec.parcial.entity.*;

public interface IRegistrosService {
	
	Personal obtenerPersonal(String codigo);
	void insertarAsistencia(Registros registros);
	void insertarPersonal();

}
