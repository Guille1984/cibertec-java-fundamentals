package edu.pe.cibertec.parcial.service;

import edu.pe.cibertec.parcial.entity.*;
import edu.pe.cibertec.parcial.repo.Data;

public class RegistrosServiceImpl implements IRegistrosService {
	
	Data data = new Data();

	@Override
	public void insertarAsistencia(Registros registros) {
		data.insertarAsistencia(registros);
	}

	@Override
	public Personal obtenerPersonal(String codigo) {
		return data.obtenerPersonal(codigo);
	}

	@Override
	public void insertarPersonal() {
		data.insertarPersonal();
	}

}