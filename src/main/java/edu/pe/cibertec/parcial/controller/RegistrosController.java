package edu.pe.cibertec.parcial.controller;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Scanner;
import org.springframework.stereotype.Component;
import edu.pe.cibertec.parcial.entity.*;
import edu.pe.cibertec.parcial.service.*;
import edu.pe.cibertec.parcial.utils.Excepciones;

@Component
public class RegistrosController {
	
	IRegistrosService servicio = new RegistrosServiceImpl();
	
	boolean flag=true;
	
	public Personal obtenerPersonal(String codigo) {
		return servicio.obtenerPersonal(codigo);
	}
	
	public void insertarAsistencia(Registros registros) {
		servicio.insertarAsistencia(registros);
	}
	
	public void insertarPersonal() throws Excepciones {
		
		@SuppressWarnings("resource")
		Scanner sc = new Scanner(System.in);
		
		if(flag) {
			servicio.insertarPersonal();
		}
		flag=false;
        String codigo = sc.nextLine();
		System.out.println("codigo es: " + codigo);
		Personal p = obtenerPersonal(codigo);
		if(p!=null) {
			System.out.println("Bienvenid@ : " + p.getNombre());
			LocalDateTime date = LocalDateTime.now();
			DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd-MM-yyyy HH:mm:ss");
	        String formatDateTime = date.format(formatter);
			System.out.println("Fecha de hoy: " + formatDateTime);
			String asistencia=null;
			if(Integer.parseInt(formatDateTime.substring(11, 13)) <= 8 && formatDateTime.substring(14,16).equals("00")) {
				asistencia = "PUNTUAL";
			}
			else {
				asistencia = "TARDE";
			}
			Registros r = new Registros(p.getTipo(),p,formatDateTime,asistencia);
			insertarAsistencia(r);
			System.out.println("Asistencia registrada correctamente");
			insertarPersonal();
		}
		else {
			System.out.println("Personal no encontrado !!");
		}
		
	}
}