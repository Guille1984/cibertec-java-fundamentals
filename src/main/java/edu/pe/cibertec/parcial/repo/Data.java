package edu.pe.cibertec.parcial.repo;

import java.util.ArrayList;
import java.util.List;
import org.springframework.stereotype.Repository;
import edu.pe.cibertec.parcial.entity.*;

@Repository
public class Data {
	
	private List<Personal> listaPersonal = new ArrayList<>();
	private List<Registros> listaAsistencias = new ArrayList<>();

	public void insertarPersonal() {
		
		Personal socio01 = new Socio("S001","Gabriel Diaz","Av.Rotonda # 134","Socio","Anual");
		Personal socio02 = new Socio("S002","Olga Carreño","Av.Javier Prado # 3234","Socio","Trimestral");
		Personal socio03 = new Socio("S003","Juan Pérez","Av.Tomás Marsano # 1722","Socio","Mensual");
		
		Personal instructor01 = new Instructor("I001","Karina Cherres","Calle Wiracocha # 134","Instructor","Grupo1");
		Personal instructor02 = new Instructor("I002","Nicole Abanto","Av.Aviación # 341","Instructor","Grupo4");
		Personal instructor03 = new Instructor("I003","Paulo Neyra","Av.Puruchuco # 877","Instructor","Grupo2");
		
		Personal trabajador01 = new Trabajador("T001","Javier Polo","Av. Huaylas # 443","Trabajador","Chorrillos");
		Personal trabajador02 = new Trabajador("T002","Fiorella Gomez","Av.Caminos del Inca # 879","Trabajador","Surco");
		Personal trabajador03 = new Trabajador("T003","Ernesto Chicata","Av.San Borja Sur # 823","Trabajador","San Borja");
		
		listaPersonal.add(socio01);
		listaPersonal.add(socio02);
		listaPersonal.add(socio03);
		listaPersonal.add(instructor01);
		listaPersonal.add(instructor02);
		listaPersonal.add(instructor03);
		listaPersonal.add(trabajador01);
		listaPersonal.add(trabajador02);
		listaPersonal.add(trabajador03);
	}
	
	public Personal obtenerPersonal(String codigo) {
		Personal p = null;
		for (Personal persona : listaPersonal) {
			if(persona.getCodigo().equals(codigo)) {
				return persona;
			}
		}
		return p;
	}
	
	public void insertarAsistencia(Registros registros) {
		listaAsistencias.add(registros);
		listarAsistencias();
	}
	
	public void listarAsistencias() {
		System.out.println("**********REGISTRO*****DE******ASISTENCIAS************");
		System.out.println("**TIPO**PERSONAL*****NOMBRE******FECHA-HORA******ASISTENCIA*****");
		listaAsistencias.forEach((final Registros r)-> System.out.println(r.getPersona().getTipo() + "***-***" + r.getPersona().getNombre() + "***-***" + r.getFechaHora() + "***-***" + r.getAsistencia()));
	}
	
}