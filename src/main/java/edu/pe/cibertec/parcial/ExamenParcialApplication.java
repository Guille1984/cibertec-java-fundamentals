package edu.pe.cibertec.parcial;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import edu.pe.cibertec.parcial.controller.RegistrosController;
import edu.pe.cibertec.parcial.utils.Excepciones;

@SpringBootApplication
public class ExamenParcialApplication {
	
	public static void main(String[] args) {
		RegistrosController controlador = new RegistrosController();
		SpringApplication.run(ExamenParcialApplication.class, args);
		try {
			controlador.insertarPersonal();
		} catch (Excepciones e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		System.out.println("Mensaje de éxito: ASISTENCIA REGISTRADA CORRECTAMENTE !!");
	}

}
