package edu.pe.cibertec.parcial.entity;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class Personal {
	
	protected String codigo;
	protected String nombre;
	protected String direccion;
	protected String tipo;

}