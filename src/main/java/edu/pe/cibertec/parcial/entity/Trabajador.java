package edu.pe.cibertec.parcial.entity;

public class Trabajador extends Personal {
	
	private String lugarTrabajo;

	public Trabajador(String codigo, String nombre, String direccion, String tipo, String lugarTrabajo) {
		super(codigo, nombre, direccion, tipo);
		this.lugarTrabajo = lugarTrabajo;
	}

	public String getLugarTrabajo() {
		return lugarTrabajo;
	}

	public void setLugarTrabajo(String lugarTrabajo) {
		this.lugarTrabajo = lugarTrabajo;
	}

}