package edu.pe.cibertec.parcial.entity;

public class Socio extends Personal {
	
	private String membresia;

	

	public Socio(String codigo, String nombre, String direccion, String tipo, String membresia) {
		super(codigo, nombre, direccion, tipo);
		this.membresia = membresia;
	}

	public String getMembresia() {
		return membresia;
	}

	public void setMembresia(String membresia) {
		this.membresia = membresia;
	}

}