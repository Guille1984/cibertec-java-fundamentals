package edu.pe.cibertec.parcial.entity;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class Registros {
	
	private String tipoPersonal;
	private Personal persona;
	private String fechaHora;
	private String asistencia;

}
