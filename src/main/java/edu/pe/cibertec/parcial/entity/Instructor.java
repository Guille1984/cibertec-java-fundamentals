package edu.pe.cibertec.parcial.entity;

public class Instructor extends Personal {
	
	private String grupoAlumnos;

	public Instructor(String codigo, String nombre, String direccion, String tipo, String grupoAlumnos) {
		super(codigo, nombre, direccion, tipo);
		this.grupoAlumnos = grupoAlumnos;
	}

	public String getGrupoAlumnos() {
		return grupoAlumnos;
	}

	public void setGrupoAlumnos(String grupoAlumnos) {
		this.grupoAlumnos = grupoAlumnos;
	}

}